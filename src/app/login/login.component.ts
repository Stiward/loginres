import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../app/servicio/login.service';
import { Router } from '@angular/router';
import { Http } from '@angular/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  constructor(private loginService: LoginService, private router: Router, private http: Http) { }

  logIn(username: string, password: string, event: Event) {
    event.preventDefault(); // Avoid default action for the submit button of the login form

    // Calls service to login user to the api rest
    this.loginService.login(username, password).subscribe(

      res => {
       console.log(res);

      },
      error => {
        console.error(error);
        
      },

      () => this.navigate()
    );
  }

  navigate() {
    this.router.navigateByUrl('/home');
  }


  ngOnInit() {
    this.loginService.login('peter@klaven', 'cityslicka').subscribe(
      res => {
        console.log(res);
    });
  }

}